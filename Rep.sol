contract Rep {

    event registerEvent(address indexed user, bytes32 message, bool result);

    modifier isAdmin() {
        if (msg.sender == admin) {
            _
        }
    }

    modifier eventModifier(address _user, bytes32 _message, bool _result) {
        _
        registerEvent(_user, _message, _result);
    }

    function Rep(address _owner_1, address _owner_2) {
        admin = msg.sender;
        //currently hardcoded. See "total_owners" as well.
        owners[0] = _owner_1;
        owners[1] = _owner_2;
    }

    function submitVerifiedLink(bytes _link) external eventModifier(msg.sender, "submit.VerifiedLink", true) {
        users[msg.sender].verifiedLink = _link;
    }

    function setVerified(address _for, bool _status) isAdmin eventModifier(_for, "changedVerified", _status) {
        users[_for].verified = _status; //oracle sets verified or not
    }

    function createRep(address _for, uint _amount) private eventModifier(_for, "rep.created", true) {
        users[_for].balance += _amount; //make sure, 0 is default.
    }

    function unlockRep(address _for, bytes32 _reward) isAdmin {
        createRep(_for, rewards[_reward]);
    }

    function setReward(bytes32 _action, uint _amount) isAdmin {
        rewards[_action] = _amount;
    }

    //send rep to another.
    function beam(address _to, uint _amount) {
        if (_amount > 0) {
            if(users[msg.sender].balance >= _amount) {
                users[msg.sender].balance -= _amount;
                users[_to].balance += _amount;

                registerEvent(msg.sender, "beam.sent", true);
                registerEvent(_to, "beam.received", true);
            }
        }

        link(msg.sender, _to);
    }

    //at every beam, see if they've connected. If not & verified, issue 1 token each.
    function link(address _from, address _to) private {
        if(users[_from].verified == true && users[_to].verified == true) {
            //Not really necessary to check both, but might want to async in the future.
            if(links[_from][_to] == false && links[_to][_from] == false) {
                links[_from][_to] = true;
                links[_to][_from] = true;

                registerEvent(_from, "link.from", true);
                registerEvent(_to, "link.to", true);

                createRep(_from, 1);
                createRep(_to, 1);
            }
        }
    }

    //changing the administrator requires 'n' amount of owners to prime it, and then execute it.
    //this is a mitigation strategy in the event that the administrator is compromised.
    function primeAdminChange(address _newAdmin) {
        for (uint i = 0; i < total_owners; i+=1) {
            if(owners[i] == msg.sender) {
                primed[msg.sender] = _newAdmin;
            }
        }
    }

    function unprimeAdminChage() {
        for (uint i = 0; i < total_owners; i+=1) {
            if(owners[i] == msg.sender) {
                primed[msg.sender] = 0x0;
            }
        }
    }

    function adminChange(address _newAdmin) returns (bool) {
        address checkingAddress;
        for(uint i = 0; i < total_owners; i+=1) {
            if(primed[owners[i]] == 0x0) {
                return false; //one owner did not prime a new admin.
            }

            if(i == 0) {
                checkingAddress = primed[owners[i]];
            } else {
                if(primed[owners[i]] != checkingAddress) {
                    return false; //owners didn't match primed admin.
                }
            }
        }

        admin = _newAdmin;
        //reset primes
        for(i=0; i < total_owners; i+=1) {
            primed[owners[i]] = 0x0;
        }
        return true;
    }

    struct user {
        uint balance;
        bool verified;
        bytes verifiedLink;
    }

    mapping (bytes32 => uint) rewards;
    mapping (address => user) public users;
    mapping (address => mapping (address => bool)) public links;
    address public admin;
    address[] owners;
    mapping (address => address) primed;
    uint constant total_owners = 2;
} 
